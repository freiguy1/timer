# STM32 Timer project with rust

![Image of project](./image.jpg "Working prototype")

## Setup info & requirements

At the time of this writing, this repository is working with these versions of rust and cargo:

```
rustc 1.30.0-nightly (5c875d938 2018-09-24)
cargo 1.31.0-nightly (de314a8b2 2018-09-21)
```

Also main's `Cargo.toml` file is currently using a version of RTFM that comes from a github PR branch which is a bit more up to date. That PR will eventually go away so I'll need to watch/update that.

This repo also includes a `.vscode` folder which has some configuration files that allows for in-editor debugging. It's not working very reliably yet.

To get everything compiling and working, a decent amount of setup is required, and here isn't the place for that. Refer to [The Embedded Rust Book](https://rust-embedded.github.io/book/) for up to date setup instructions.

## About the project

The goal is an ultra minimal timer. For the user interface, it's only 4 buttons:

- \+1 minute - _adds one minute to the timer_
- \+5 minutes - _adds five minutes to the timer_
- \+15 minutes - _adds fifteen minutes to the timer_
- clear - _clears the current timer_

For the output, it's a buzzer and maybe an LED for battery status.

It's pretty self explanitory, but here are the basic instructions. Press the + buttons until it adds up to the amount you want to time. At this point, the timer will begin counting down. At any point you can clear or add more time to the currently counting timer. When the timer reaches 0, an alarm will sound.

For example: if you want to time 37 minutes, you'd press the +15 minute twice, +5 button once, and +1 button twice.

I would love for this device to live on one AAA battery for years - not sure if that's feasable, but the vast, vast majority of the time, the device should be sleeping and waiting for an interrupt.

## Project structure

There are two main directories: `lib` and `main`.

### lib directory

This directory contains all the logic parts of the application. It is agnostic of hardware and because of this it can be unit tested. However the logic part does need some channel for communicating with the hardware. So I made a rust trait called `Ashi` (short for application specific hardware interface). It provides several simple methods so the logic portion can interact with the hardware.  Then a dev simply implements the Ashi for their controller, and the logic can control any board.  Here are some example functions:

```rust
fn start_beep(&mut self);
fn stop_beep(&mut self);
fn start_sys_timer(&mut self);
fn stop_sys_timer(&mut self);
```

### main directory

This directory contains the runnable program which compiles for the blue pill. There is an implementation for the Ashi (defined in the lib directory). The code that exists in this project is relatively simple and pass through. The lib project handles the actual thought of the timer.

