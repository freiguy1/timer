#![deny(unsafe_code)]
#![deny(warnings)]
#![no_std]
#![no_main]

extern crate cortex_m;
extern crate cortex_m_rt as rt;
extern crate cortex_m_rtfm as rtfm;
extern crate cortex_m_semihosting;
extern crate panic_abort;
extern crate stm32f103xx_hal as hal;

extern crate lib;

mod blue_pill_ashi;

// use core::fmt::Write;
// use cortex_m_semihosting::hio::{self};//, HStdout};
use cortex_m::peripheral::syst::SystClkSource;
use rtfm::{app, Threshold};
use hal::prelude::*;
use hal::gpio::{Output, PushPull};
use hal::gpio::gpioc::PC13;
use blue_pill_ashi::BluePill as BPAshi;

app! {
    device: hal::stm32f103xx,
    resources: {
        static ON: bool = false;
        static LED_PIN: PC13<Output<PushPull>>;
        // static STDOUT: HStdout;
        static EXTI: hal::stm32f103xx::EXTI;
        static ASHI: BPAshi;
        static STATE: lib::TimerState;
    },
    tasks: {
        SysTick: {
            path: sys_tick,
            resources: [ON, LED_PIN, ASHI, STATE],
        },
        EXTI15_10: {
            path: button_pressed,
            resources: [EXTI, STATE, ASHI]
        }
    }
}

#[allow(unsafe_code)]
fn init(mut p: init::Peripherals, _r: init::Resources) -> init::LateResources {
    let mut rcc_constrained = p.device.RCC.constrain();
    let mut flash_constrained = p.device.FLASH.constrain();
    let mut afio_constrained = p.device.AFIO.constrain(&mut rcc_constrained.apb2);
    let clocks = rcc_constrained.cfgr.freeze(&mut flash_constrained.acr);

    let gpioa = p.device.GPIOA.split(&mut rcc_constrained.apb2);
    let mut gpiob = p.device.GPIOB.split(&mut rcc_constrained.apb2);
    let mut gpioc = p.device.GPIOC.split(&mut rcc_constrained.apb2);

    // Initialize LED pin
    let led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

    // ------------------------------------------------------

    // External interrupt setup for buttons
    // Set pb10 and pb11 to input with pull up resistor
    gpiob.pb10.into_floating_input(&mut gpiob.crh);
    gpiob.pb11.into_floating_input(&mut gpiob.crh);
    gpiob.pb12.into_floating_input(&mut gpiob.crh);

    // Enable the alternate function I/O clock (for external interrupts)
    let rcc = unsafe { &*hal::stm32f103xx::RCC::ptr() };
    rcc.apb2enr.modify(|_, w| w.afioen().enabled());
    // Set external interrupt 10, 11, 12, 13 multiplexers to use port B
    afio_constrained.exticr3.exticr3().modify(|_, w| unsafe {
        w.exti10().bits(0x01).exti11().bits(0x01)
    });
    afio_constrained.exticr4.exticr4().modify(|_, w| unsafe {
        w.exti12().bits(0x01).exti13().bits(0x01)
    });
    // Enable interrupt on EXTI10 - EXTI13
    p.device.EXTI.imr.modify(|_, w| 
        w.mr10().set_bit()
            .mr11().set_bit()
            .mr12().set_bit()
            .mr13().set_bit());
    // Set falling trigger selection for EXTI10 - EXTI13
    p.device.EXTI.ftsr.modify(|_, w| 
        w.tr10().set_bit()
            .tr11().set_bit()
            .tr12().set_bit()
            .tr13().set_bit());

    //---------------------------------------------------------

    // configure the system timer to generate one interrupt every second
    p.core.SYST.set_clock_source(SystClkSource::Core);
    p.core.SYST.set_reload(8_000_000); // 1s
    p.core.SYST.enable_interrupt();

    // --------------------------------------------------------

    // setting up application specific hardware interface
    let ashi = BPAshi::init(
        gpioa, p.device.TIM2, &mut afio_constrained.mapr,
        &mut rcc_constrained.apb1, clocks, p.core.SYST);

    // --------------------------------------------------------

    // writeln!(hio::hstdout().unwrap(), "hsitrim: {:?}", rcc.cr.read().hsitrim().bits()).unwrap();

    init::LateResources {
        LED_PIN: led,
        // STDOUT: hio::hstdout().unwrap(),
        EXTI: p.device.EXTI,
        ASHI: ashi,
        STATE: lib::TimerState::new()
    }
}

fn idle() -> ! {
    loop {
        rtfm::wfi();
    }
}

fn sys_tick(_t: &mut Threshold, mut r: SysTick::Resources) {
    *r.ON = !*r.ON;
    if *r.ON {
        r.LED_PIN.set_high();
    } else {
        r.LED_PIN.set_low();
    }

    let mut timer = lib::Timer::new(&mut *r.STATE, &mut *r.ASHI);

    timer.second_tick();
}

fn button_pressed(_t: &mut Threshold, mut r: EXTI15_10::Resources) {
    let mut timer = lib::Timer::new(&mut *r.STATE, &mut *r.ASHI);
    if r.EXTI.pr.read().pr10().bit() {
        r.EXTI.pr.modify(|_, w| w.pr10().set_bit());
        timer.button_1_click();
    } else if r.EXTI.pr.read().pr11().bit() {
        r.EXTI.pr.modify(|_, w| w.pr11().set_bit());
        timer.button_2_click();
    } else if r.EXTI.pr.read().pr12().bit() {
        r.EXTI.pr.modify(|_, w| w.pr12().set_bit());
        timer.button_3_click();
    } else if r.EXTI.pr.read().pr13().bit() {
        r.EXTI.pr.modify(|_, w| w.pr13().set_bit());
        timer.button_4_click();
    }
}
