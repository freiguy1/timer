// Application Specific Hardware Interface

use hal::prelude::*;
use hal::pwm::{C1, Pwm};
use hal::stm32f103xx::TIM2;
use cortex_m::peripheral::SYST;
use lib::Ashi;

pub struct BluePill {
    // stdout: HStdout,
    beep_pwm: Pwm<TIM2, C1>,
    beep_pwm_max_duty: u16,
    syst: SYST
}

impl BluePill {
    pub fn new(beep_pwm: Pwm<TIM2, C1>, syst: SYST) -> Self {
        let beep_pwm_max_duty = beep_pwm.get_max_duty();
        BluePill {
            beep_pwm,
            beep_pwm_max_duty,
            syst
        }
    }

    pub fn init(mut gpioa: ::hal::gpio::gpioa::Parts,
        tim2: ::hal::stm32f103xx::TIM2,
        mapr: &mut ::hal::afio::MAPR,
        apb1: &mut ::hal::rcc::APB1,
        clocks: ::hal::rcc::Clocks,
        syst: SYST) -> Self {
        // PWM
        // let mut gpioa = p.device.GPIOA.split(&mut rcc_constrained.apb2);
        let a0 = gpioa.pa0.into_alternate_push_pull(&mut gpioa.crl);
        let mut beep_pwm = tim2
            .pwm(
                a0,
                mapr,
                2048.hz(),
                clocks,
                apb1,
            );
        beep_pwm.enable();
        let beep_pwm_max_duty = beep_pwm.get_max_duty();
        BluePill {
            beep_pwm,
            beep_pwm_max_duty,
            syst
        }
    }
}

impl Ashi for BluePill {
    fn start_beep(&mut self) {
        self.beep_pwm.set_duty(self.beep_pwm_max_duty / 2);
    }

    fn stop_beep(&mut self) {
        self.beep_pwm.set_duty(0);
    }

    fn start_syst(&mut self) {
        self.syst.clear_current();
        self.syst.enable_counter();
    }

    fn stop_syst(&mut self) {
        self.syst.disable_counter()
    }
}
