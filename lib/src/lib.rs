#![no_std]

const NUM_BEEPS: u8 = 1;

// Application Specific Hardware Interface

pub trait Ashi {
    fn start_beep(&mut self);
    fn stop_beep(&mut self);
    fn start_syst(&mut self);
    fn stop_syst(&mut self);
}

pub struct TimerState {
    // LED_ON: bool,
    countdown: Option<u16>,
    beep_counter: Option<u8>
}

impl TimerState {
    pub fn new() -> Self {
        TimerState {
            countdown: None,
            beep_counter: None
        }
    }
}

pub struct Timer<'a> {
    state: &'a mut TimerState,
    ashi: &'a mut Ashi
}

impl<'a> Timer<'a> {
    pub fn new<T>(state: &'a mut TimerState, ashi: &'a mut T) -> Self
        where T: Ashi
    {
        Timer { state, ashi }
    }

    pub fn second_tick(&mut self) {
        match self.state.countdown {
            Some(1) => {
                // writeln!(r.STDOUT, "Buzzer goes off and then timer is done").unwrap();
                self.state.countdown = None;
                self.state.beep_counter = Some(0);
            },
            Some(c) => {
                // writeln!(r.STDOUT, "{:?} second(s) left", c - 1).unwrap();
                self.state.countdown = Some(c - 1);
            }
            None => { }
        };

        if let Some(bc) = self.state.beep_counter {
            let new_bc = bc + 1;
            self.state.beep_counter = Some(new_bc);
            if new_bc % 2 == 0 { // If even, rest
                self.ashi.stop_beep();
                if new_bc == NUM_BEEPS * 2 {
                    self.state.beep_counter = None;
                    self.ashi.stop_syst();
                }
            } else { // If odd, play buzzer
                self.ashi.start_beep();
            }
        }
    }

    pub fn button_1_click(&mut self) {
        self.stop_beeping();
        self.add_seconds(1);
    }

    pub fn button_2_click(&mut self) {
        self.stop_beeping();
        self.add_seconds(5);
    }

    pub fn button_3_click(&mut self) {
        self.stop_beeping();
        self.add_seconds(10);
    }

    pub fn button_4_click(&mut self) {
        // Reset
        self.stop_beeping();
        self.state.countdown = None;
        self.ashi.stop_syst();
    }

    fn stop_beeping(&mut self) {
        self.state.beep_counter = None;
        self.ashi.stop_beep();
    }

    fn add_seconds(&mut self, seconds: u16) {
        let new_countdown = self.state.countdown.unwrap_or(0) + seconds;
        if new_countdown == 0 { return } // short circuit if this is the case
        if self.state.countdown.is_none() {
            // If not currently counting down - start system timer
            self.ashi.start_syst();
        }
        self.state.countdown = Some(new_countdown);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct MockAshi;

    impl Ashi for MockAshi {
        fn start_beep(&mut self) {}
        fn stop_beep(&mut self) {}
        fn start_syst(&mut self) {}
        fn stop_syst(&mut self) {}
    }

    #[test]
    fn countdown_decreases() {
        let mut state = TimerState {
            countdown: Some(10),
            beep_counter: None
        };
        let mut ashi = MockAshi {};
        let mut timer = Timer::new(&mut state, &mut ashi);
        timer.second_tick();
        assert_eq!(state.countdown, Some(9));
    }

}
